# this code may not be used for any purpose. be gay, do crime (A)

import argparse
import os
import subprocess
import sys

PHONY = []
RULES = []

class rule:
    """Decorator to define a build rule."""

    def __init__(self, target, *sources, **kwargs):
        """Create a rule.

        target: What this rule builds. Include a % to make a Pattern Rule.
        sources: What this rule needs. Include a % to make a Pattern Rule.
        kwargs: options
            phony (default False)
            tfile (default None) - pass target as an open file - specify mode
            sfile (default None) - pass sources as open files - specify mode

        Should be called as a decorator over a function rule(target, sources).
        """
        self.target = target
        self.sources = sources
        self.options = kwargs

        # Memoize build_needs - it gets called a lot
        self._needs_memo = {}

    def __call__(self, f):
        """Evaluate the decorator"""

        self.f = f
        RULES.append(self)
        if self.phony:
            PHONY.append(self)
        return self.build

    @property
    def phony(self):
        return self.options.get("phony", False)

    def build_needs(self, target):
        """Check if this rule can build the target. Returns a list of required
        sources with patterns resolved if it can (or an empty list of it needs
        no sources). If it can't build the target, returns None.

        This does not check if the required sources are available.
        """

        memo = self._needs_memo.get(target, 0)
        if memo is not 0:
            return memo

        substr = _pattern_match(self.target, target)

        if substr is None:
            needs = None
        else:
            needs = [_pattern_sub(i, substr) for i in self.sources]

        self._needs_memo[target] = needs
        return needs

    def build(self, target):
        """Build the specified target. Should only be called if build_needs()
        previously returned non-None for this target.
        """

        needs = self.build_needs(target)

        if needs is None:
            raise Exception("(internal) called impossible .build()")

        tfile = self.options.get("tfile", None)
        sfile = self.options.get("sfile", None)

        if tfile is not None:
            call_target = open(target, tfile)
        else:
            call_target = target

        if sfile is not None:
            call_needs = [open(i, sfile) for i in needs]
        else:
            call_needs = needs

        try:
            self.f(call_target, call_needs)
        finally:
            if tfile is not None:
                call_target.close()
            if sfile is not None:
                for i in call_needs:
                    i.close()

    def check_build(self, target):
        """Return whether the target needs to be built."""

        if self in PHONY:
            return True

        if not os.path.exists(target):
            return True

        needs = self.build_needs(target)
        mtime = os.stat(target).st_mtime

        for i in needs:
            if not os.path.exists(i):
                return True
            if os.stat(i).st_mtime > mtime:
                return True

        return False

def rm(path):
    try:
        os.unlink(path)
    except FileNotFoundError:
        pass

def _pattern_match(pattern, path):
    """Test whether a pattern matches a path.

    Returns:
        The % substring, if it matches and there is one.
        Empty string, if it matches but there is no %.
        None, if it does not match.
    """

    pre, delim, post = pattern.partition("%")

    if not delim:
        return "" if (pattern == path) else None
    else:
        if path.startswith(pre) and path.endswith(post):
            return path[len(pre):-len(post)]
        else:
            return None

def _pattern_sub(pattern, substring):
    """Insert a %-substring into a pattern. If the pattern does not contain a
    %, nothing is inserted.
    """

    pre, delim, post = pattern.partition("%")

    if not delim:
        return pattern
    else:
        return pre + substring + post

class _buildstep:
    def __init__(self, **kwargs):
        """Valid kwargs:

        item: the item being built
        rule: the rule it's built from
        """

        self.d = {}
        for k, v in kwargs.items():
            self.d[k] = v
            setattr(self, k, v)

    def __repr__(self):
        return f"_buildstep(**{self.d!r})"

def _eval_deps(target, rules):
    """Find the shortest path to target following rules.

    Returns a list of _buildstep(item, rule) showing that path. If the item
    can't be built, returns None.
    """

    candidates = []

    for rule in rules:
        needs = rule.build_needs(target)
        if needs is None:
            continue
        elif needs == []:
            candidates.append([
                _buildstep(item=target, rule=rule)
            ])
        else:
            dead_end = False
            chain = []
            for need in needs:
                try:
                    subchain = _eval_deps(need, rules)
                except RecursionError:
                    dead_end = True
                    break
                if subchain is None:
                    dead_end = True
                    break
                else:
                    chain.extend(subchain)
            if dead_end:
                continue
            else:
                candidates.append([
                    _buildstep(item=target, rule=rule)
                ] + chain)

    if candidates:
        candidates.sort(key=lambda x: len(x))
        return candidates[-1]
    else:
        return None


def run():
    """Run the build."""

    ap = argparse.ArgumentParser(
        description="Better mAKE",
    )
    ap.add_argument(
        "target",
        default=["all"],
        nargs="*",
        help="Target to build (default: all)",
    )
    ap.add_argument(
        "-n", "--dry-run",
        dest="n",
        action="store_true",
        help="Don't build, only print what would be built",
    )
    args = ap.parse_args()

    for target in args.target:
        deps = _eval_deps(target, RULES)

        for step in deps[::-1]:
            if step.rule.check_build(step.item):
                if args.n:
                    print(step.item)
                else:
                    step.rule.build(step.item)
